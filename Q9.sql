use Q2;

select
item_category.category_name,SUM(item_price) AS total_price
from
item
inner join
item_category
on
item.category_id=item_category.category_id
group by
category_name;
